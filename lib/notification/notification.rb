class Notification
  require 'date'
  require 'terminal-notifier'

  def initialize(user_input, word)
    threads = []
    # frequency of notifications in minutes
    notification_increments = [5, 10, 30]

    # convert to seconds
    notification_increments.map! {|item| item * 60}

    3.times do |i|
       Thread.start {
        sleep(notification_increments[i])
        notify(user_input, word)
      }
    end
  end

  # other arguments include:
  # group: 'Vocabulary Notifier'
  # sound: 'Hero',
  # activate: 'com.apple.Terminal'
  def notify(user_input, word)
    TerminalNotifier.notify(word,
      title: user_input)
  end
end
