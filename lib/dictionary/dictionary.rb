class Dictionary
  def lookup
    dict = generate_dictionary

    user_input = ''
    print "> "
    until ['q', 'Q'].include? user_input do
      user_input = STDIN.gets.chomp
      if dict.has_key?(user_input.to_sym)
        word = dict[user_input.to_sym]
        puts word
        notification = Notification.new(user_input, word)
        print '> '
      elsif user_input == "" || user_input == "\n"
        print '> '
      else
        print "#{user_input} not found\n> " unless (user_input == 'q' || user_input == 'Q')
      end
    end
  end

  def build_dictionary(dict_as_array)
    dict = Hash.new
    dict_as_array.each do |line|
      line = line.split("`") # [word, definition]
      dict[line[0].to_sym] = line[1]
    end
    dict
  end
  
  private

  def generate_dictionary
    dict_as_array = read_file_to_array('data/edict.txt')
    dict = build_dictionary(dict_as_array)
  end

  def read_file_to_array(filename)
    arr = []
    File.foreach(filename).with_index do |line|
      arr.push(line.strip!)
    end
    arr
  end
end
