# encoding: utf-8
require_relative 'dictionary/dictionary'
require_relative 'notification/notification'

dictionary = Dictionary.new
threads = []

threads << Thread.new {
  dictionary.lookup
}

threads.each { |thread| thread.join }
