Running:

Just clone the repository, navigate to the folder via terminal and run ruby lib/dictionary.rb. 

If your word is in the dictionary, you will receive a notification reminding you of the meaning in 5, 10 and 30 minutes time. These increments are currently hard coded in lib/notifiation/notification.rb and an easy way to interface with them is on the way.

Still early days. Dictionary is not complete (using JDICT) but needs further processing and updating.

![alt text](https://s32.postimg.org/6vwjssg4l/2016_07_26_11_58_59.png)

