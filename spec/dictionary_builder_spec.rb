require_relative '../lib/dictionary/dictionary.rb'

RSpec.describe do
  describe 'generating a hash with word and definition' do
    it 'builds the dictionary correctly' do
      arr = ['word`definition', 'word2`definition2']
      
      dict = Dictionary.new
      dictionary = dict.build_dictionary(arr)

      expect(dictionary).to eq ({word: 'definition', word2: 'definition2'})
    end
  end
end
